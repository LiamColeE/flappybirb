﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour
{
    public Spawner spawner;
    public BackgroundController backgroundController;
    public PlayerController playerController;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI highScoreText;
    public TextMeshProUGUI coins;
    private float score;
    private float coinCount;
    
    public float Coins
    {
        get
        {
            return coinCount;
        }
        set
        {
            coinCount = value;
            coins.text = "COINS:"+coinCount;
        }
    }

    public float Score
    {
        get
        {
            return score;
        }
        set
        {
            score = value;
            scoreText.text = "SCORE:" + score;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        try
        {
            highScoreText.text = "HIGHSCORE:" + PlayerPrefs.GetFloat("HighScore");
        }
        catch
        {
            PlayerPrefs.SetFloat("HighScore",0f);
        }

        try
        {
            coinCount = PlayerPrefs.GetFloat("Coins");
            coins.text = "COINS:" + coinCount;
        }
        catch
        {
            PlayerPrefs.SetFloat("Coins", 0f);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GameOver()
    {
        if (score > PlayerPrefs.GetFloat("HighScore"))
        {
            PlayerPrefs.SetFloat("HighScore", score);
        }
        PlayerPrefs.SetFloat("Coins", coinCount);
        spawner.gameOver = true;
        playerController.gameOver = true;
        backgroundController.gameOver = true;
        GameObject[] obstacles = GameObject.FindGameObjectsWithTag("ObstacleParent");
        foreach (GameObject obstacle in obstacles)
        {
            obstacle.GetComponent<ObstacleController>().gameOver = true;
        }
        SceneManager.LoadScene("GameOver",LoadSceneMode.Additive);
    }
}
