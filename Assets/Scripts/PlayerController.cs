﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public GameManager gameManager;
    public float jumpMagnitude;
    public bool gameOver;
    float maxVelocity = 10;
    bool canJump = true;
    Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = this.gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (gameOver)
        {
            return;
        }
        if (Input.GetMouseButtonDown(0) && canJump)
        {
            rb.velocity = new Vector2(0, 0);
            rb.AddForce(Vector3.up * jumpMagnitude);
            //canJump = false;
            //Invoke("ResetJump", .1f);
        }
    }

    void FixedUpdate()
    {
        rb.velocity = Vector2.ClampMagnitude(rb.velocity, maxVelocity);
    }

    void ResetJump()
    {
        canJump = true;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (gameOver)
        {
            return;
        }
        Debug.Log("Collison");
        if(collision.gameObject.tag == "Obstacle")
        {
            gameManager.GameOver();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Point")
        {
            GameObject.Destroy(collision.gameObject);
            gameManager.Score++;
        }
        else if (collision.gameObject.tag == "Coin")
        {
            GameObject.Destroy(collision.gameObject);
            gameManager.Coins++;
        }
    }
}
