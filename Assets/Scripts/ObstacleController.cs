﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleController : MonoBehaviour
{
    public float moveSpeed = .2f;
    public float destroyTime;
    public bool gameOver;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("Destroy", destroyTime);
    }

    // Update is called once per frame
    void Update()
    {
        if (gameOver)
        {
            CancelInvoke();
            return;
        }
        this.transform.Translate(-moveSpeed * Time.deltaTime,0,0);
    }

    private void Destroy()
    {
        GameObject.Destroy(this.gameObject);
    }
}
