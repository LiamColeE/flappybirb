﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundController : MonoBehaviour
{

    public GameObject mountain1;
    public float mountain1ScrollSpeed;
    float mountain1Offset = 0;

    public GameObject mountain2;
    public float mountain2ScrollSpeed;
    float mountain2Offset = 0;

    public GameObject cloud1;
    public GameObject cloud2;
    public bool gameOver;

    private Vector2 screenBounds;
    // Start is called before the first frame update
    void Start()
    {
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        SpawnCloud();
    }

    void SpawnCloud()
    {
        int cloud = Random.Range(0,2);

        if (cloud == 1)
        {
            GameObject cloudGO = GameObject.Instantiate(cloud1, new Vector3(screenBounds.x + 3, Random.Range(0, 4), Random.Range(1, 7)), this.transform.rotation, this.transform);
            cloudGO.GetComponent<ObstacleController>().moveSpeed= Random.Range(.2f,.5f);
        }
        else
        {
            GameObject cloudGO = GameObject.Instantiate(cloud2, new Vector3(screenBounds.x + 3, Random.Range(0, 4), 0), this.transform.rotation, this.transform);
            cloudGO.GetComponent<ObstacleController>().moveSpeed = Random.Range(.2f, .5f);
        }
        Invoke("SpawnCloud", Random.Range(5,30));
    }

    private void Update()
    {
        if (gameOver)
        {
            CancelInvoke();
            return;
        }

        mountain1Offset += (Time.deltaTime * mountain1ScrollSpeed) / 10.0f;
        mountain1.GetComponent<Renderer>().material.SetTextureOffset("_MainTex", new Vector2(mountain1Offset, 0));

        mountain2Offset += (Time.deltaTime * mountain2ScrollSpeed) / 10.0f;
        mountain2.GetComponent<Renderer>().material.SetTextureOffset("_MainTex", new Vector2(mountain2Offset, 0));
    }
}
