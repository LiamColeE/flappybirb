﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject obstacle;
    public GameObject coin;
    public float spawnSpeed = .8f;
    public bool gameOver;

    private Vector2 screenBounds;

    // Start is called before the first frame update
    void Start()
    {
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        Invoke("Spawn", 1/spawnSpeed);
    }

    private void Update()
    {
        if (gameOver)
        {
            CancelInvoke();
        }
    }

    void Spawn()
    {
        float randHeight = Random.Range(-3f,3f);
        int randCoin = Random.Range(0,100);

        GameObject obstacleGO = GameObject.Instantiate(obstacle,new Vector3(screenBounds.x + 1.5f, randHeight, 0), this.transform.rotation);

        if (randCoin > 90)
        {
            GameObject.Instantiate(coin,obstacleGO.transform);
        }

        Invoke("Spawn", 1/spawnSpeed);
    }
}
